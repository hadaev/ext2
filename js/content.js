// var storage = chrome.storage.sync;

(function ($) {
    $(document).ready(function(){

        // localStorage.removeItem("time");
        chrome.storage.local.get('time', function(data) {
            var time = data.time;
            document.getElementById('timer2').innerHTML = time;
        });

        chrome.storage.onChanged.addListener(function(changes, namespace) {
            for (var key in changes) {
                var storageChange = changes[key];

                if(key !== 'switch') document.getElementById('timer2').innerHTML = storageChange.newValue;

                console.log('Storage key "%s" in namespace "%s" changed. ' +
                    'Old value was "%s", new value is "%s".',
                    key,
                    namespace,
                    storageChange.oldValue,
                    storageChange.newValue);
            }
        });

    });
})(jQuery);
